import React, { Component } from 'react';
import './App.css';
import MainPage from './components/mainPage/mainPage';
import { Route, Switch, BrowserRouter } from 'react-router-dom';
import GoodMoodFood from './components/containers/goodMoodFood/goodMoodFood';
import AboutUs from "./components/containers/aboutUs/aboutUs";
import Contacts from "./components/containers/contacts/contacts";

class App extends Component {
  render() {
    return (
      <div className="App">
       <BrowserRouter>
         <Switch>
           <Route path="/goodMoodFood" component={GoodMoodFood}/>
           <Route path="/AboutUs" component={AboutUs}/>
           <Route path="/contacts" component={Contacts}/>
           <Route path="/" exact component={MainPage}/>
         </Switch>
       </BrowserRouter>
      </div>
    );
  }
}

export default App;
