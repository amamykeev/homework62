import React, {Component} from 'react';
import '../mainPage/mainPage.css';
import '../stylesheet/infoBlock.css';
import '../stylesheet/mainPic.css';
import '../stylesheet/motivationalQuote.css';
import '../stylesheet/headerCss.css';
import {NavLink} from "react-router-dom";
import {GoodMoodFood} from '../containers/goodMoodFood/goodMoodFood';
import {AboutUs} from '../containers/aboutUs/aboutUs';
import {Contacts} from '../containers/contacts/contacts';
import barbell from '../pictures/barbell.jpg';
import two  from '../pictures/two.jpg';
import three  from '../pictures/three.jpg';
import four  from '../pictures/four.jpg';
import five  from '../pictures/five.jpg';
import six  from '../pictures/six.jpg';
import one  from '../pictures/one.jpg';

class MainPage extends Component {
    render() {
        return (
            <div className="metroFlex">
                    <header>
                        <div className="container">
                        <a href="#">Metroflex</a>
                        <nav className="mainNav">
                            <a>Home</a>
                            <NavLink to="GoodMoodFood">Good Mood Food</NavLink>
                            <NavLink to="AboutUs">About Us</NavLink>
                            <NavLink to="Contacts">Contacts</NavLink>
                        </nav>
                        </div>
                    </header>
                <div className="motivationalQuote">
                    <p>The mind is everything. What you think, you become.</p>
                    <span>817-465-9331</span>
                </div>
                <div className="mainPic" style={{height: '600px'}}>
                    <img src={barbell} alt="" style={{height: '600px', width: '100%'}}/>
                    <h1>original metroflex gym bishkek</h1>
                </div>
                <div className="infoBlock">
                    <h3>Metroflex Gym Rates</h3>
                    <p>Metroflex Gym Membership-No down payment or initiation
                        $30.00 plus tax per Month, Can be canceled anytime with a 30 day notice
                        Full Line of Equipment, including:
                        Titan, Paramount, Flexsteel, Steel Flex,Champion,BSN,Cobra,Extreme Fitness,MAC,MTS,Hammer Strength
                        Dumbbells from 5 to 250 lbs.-Exercise bikes by Lifecycle & Tunturi - Treadmills by Quinton & StarTrac
                        Boxing equipment by Everlast & Ringside
                        Contact Brian at 817-465-9331 or via email at brian@metroflexgym.com for more information.
                        ​It's an instant beginning to an intense training regime!</p>
                </div>
                <div className="picturesBlock">
                    <img src={one} style={{width: '50%'}}/>
                    <img src={two} style={{width: '50%'}}/>
                    <img src={three} style={{width: '50%'}}/>
                    <img src={four} style={{width: '50%'}}/>
                    <img src={five} style={{width: '50%'}}/>
                    <img src={six} style={{width: '50%'}}/>
                </div>
                <div className="infoBlock2">
                    <p>info</p>
                </div>
                <footer>
                    <p>Contacts navigation map</p>
                </footer>
            </div>
        );
    }
}

export default MainPage;