import React, {Component} from 'react';
import {NavLink} from "react-router-dom";
import {MainPage} from "../../mainPage/mainPage";

class GoodMoodFood extends Component {

    render() {
        return (
            <div>
               <header>
                   <header>
                       <div className="container">
                           <a href="#">Metroflex</a>
                           <nav className="mainNav">
                               <NavLink to="/">Home</NavLink>
                               <NavLink to="GoodMoodFood">Good Mood Food</NavLink>
                               <NavLink to="AboutUs">About Us</NavLink>
                               <NavLink to="Contacts">Contacts</NavLink>
                           </nav>
                       </div>
                   </header>
               </header>
                <div
                className="container">
                <div className="gmf">
                    <h1>You are, what you eat. Healthy food.</h1>
                    <p> You are what you eat. We think it's right.
                        Any food is food for the whole organism each cell
                        so our state depends on the food we eat.
                        We prefer healthy food. </p>
                </div>
                </div>
            </div>
        );
    }
}

export default GoodMoodFood;