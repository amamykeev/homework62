import React, {Component} from 'react';
import './aboutUs.css';
import {NavLink} from "react-router-dom";
import {MainPage} from "../../mainPage/mainPage";
import abtsone from '../../pictures/abtsone.jpg';
import abtstwo from '../../pictures/abtstwo.jpg';

class AboutUs extends Component {

    render() {
        return (
            <div>
                <header>
                    <div className="container">
                        <a href="#">Metroflex</a>
                        <nav className="mainNav">
                            <NavLink to="/">Home</NavLink>
                            <NavLink to="GoodMoodFood">Good Mood Food</NavLink>
                            <NavLink to="AboutUs">About Us</NavLink>
                            <NavLink to="Contacts">Contacts</NavLink>
                        </nav>
                    </div>
                </header>
                <div className="container">
                <div className="abts">
                    <div className="block1">
                        <img src={abtsone}/>
                        <img src={abtstwo}/>
                    </div>
                    <div className="block2">
                        <p>Metroflex Gym is not a health spa. We are a serious training facility that caters to competitive body builders.
                            Year in and year out Metroflex Gym consistently produces more winners than all the fitness centers combined.
                            You will receive free help with all aspects of contest preparation, including diet and posing routines.
                            Why go to a public fitness center where your presence intimidates others,
                            when you can go to a work-out facility tailored for professional bodybuilders like yourself?</p>
                            ​
                            <p>Training ground of over 100 bodybuilding and power-lifting champions,
                            including: Mr. Olympia: Ronnie Coleman and Branch Warren</p>
                    </div>
                </div>
                </div>
            </div>
        );
    }
}

export default AboutUs;
