import React, {Component} from 'react';
import './contacts.css';
import {NavLink} from "react-router-dom";
import {MainPage} from "../../mainPage/mainPage";

class Contacts extends Component {

    render() {
        return (
            <div>
                    <header>
                        <div className="container">
                            <a href="#">Metroflex</a>
                            <nav className="mainNav">
                                <NavLink to="/">Home</NavLink>
                                <NavLink to="GoodMoodFood">Good Mood Food</NavLink>
                                <NavLink to="AboutUs">About Us</NavLink>
                                <NavLink to="Contacts">Contacts</NavLink>
                            </nav>
                        </div>
                    </header>
                <div className="container">
                <div className="cntcts">
                    <h1>Contacts</h1>
                    <p>Contact Brian Dobson for information or call the gym directly at: 817-465-9331.</p>
                </div>
                </div>
            </div>
        );
    }
}

export default Contacts;